<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <div class="logo">
        <a href="" class="simple-text logo-normal">
          Rest Client
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <!-- your sidebar here -->
          <li class="nav-item active ">
            <a class="nav-link" href="<?= site_url('barang/tambah') ?>">
              <i class="material-icons">note_add</i>
              <p>Add Data</p>
            </a>
          </li>
          <li class="nav-item ">
            <a class="nav-link" href="<?= site_url('barang') ?>">
              <i class="material-icons">content_paste</i>
              <p>Product Table</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('invoices') ?>">
              <i class="material-icons">assignment</i>
              <p>Invoices Table </p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Add Items</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">notifications</i> Notifications
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log out</a>
                </div>
              </li>
              <!-- your navbar here -->
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <!-- your content here -->
          <div class="row">
            <div class="col-md-12">

            <!-- Breadcumb -->
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item "><a href="<?= site_url('admin') ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Add Item</li>
              </ol>
            </nav>
            
              <div class="card">
                <div class="card-header card-header-primary">
                    <h4 class="card-title ">Add Item Data</h4>
                    <p class="card-category"> Complete your item data</p>
                </div>
                <div class="card-body">
                <div class="text-danger"><?= validation_errors() ?></div>
                <?= form_open('barang/tambah'); ?>
                  <div class="form">
                    <div class="row ml-1 mr-1">
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="nama_barang" class="bmd-label-floating">Name of Item</label>
                          <input type="text" id="nama_barang" name="nama_barang" class="form-control">
                        </div>
                      </div>
                    </div>
                      <div class="row ml-1 mr-1">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="merk" class="bmd-label-floating">Brand</label>
                            <input type="text" id="merk" name="merk" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="kategori" class="bmd-label-floating">Category</label>
                            <input type="text" id="kategori" name="kategori" class="form-control">
                          </div>
                        </div>
                      </div>
                      <div class="row ml-1 mr-1">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="jumlah" class="bmd-label-floating">Quantity</label>
                            <input type="text" id="jumlah" name="jumlah" class="form-control">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="garansi" class="bmd-label-floating">Warranty</label>
                            <input type="text" id="garansi" name="garansi" class="form-control">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="harga" class="bmd-label-floating">Price</label>
                          <input type="number" id="harga" name="harga" class="form-control">
                        </div>
                      </div>
                      <div class="row ml-3 mr-1">
                        <div>
                        <label for="harga" class="bmd-label-floating">Picture</label>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div>
                                  <span class="btn">
                                    <input type="file" name="gambar" id="gambar" for="gambar" />
                                  </span>
                                </div>
                            </div>
                        </div>
                      </div>
                      <?= form_error('tabel_produk') ?>
                      <div class="pull-right">
                        <div class="form-group">
                         <button type="submit" name="submit" id="submit" class="btn btn-primary">
                         <i class="material-icons">note_add</i> save
                          <span></span>
                         </button>
                          <a href="<?= site_url('barang/index') ?>">
                          <button type="button" class="btn">Cancel</button>
                          </a>
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="">
                  REKWEB
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script> built by
            <a href="" target="_blank">Selembung</a>
          </div>
          <!-- your footer here -->
        </div>
      </footer>
    </div>
  </div>