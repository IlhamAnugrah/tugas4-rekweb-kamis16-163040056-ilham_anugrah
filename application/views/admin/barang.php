  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <div class="logo">
        <a href="" class="simple-text logo-normal">
          Rest Client
        </a>
      </div>
      <div class="sidebar-wrapper" id="myDIV">
        <ul class="nav">
          <li class="nav-item  ">
            <a class="nav-link" href="<?= site_url('barang/tambah') ?>">
              <i class="material-icons">note_add</i>
              <p>Add Data</p>
            </a>
          </li>
          <li class="nav-item active ">
            <a class="nav-link" href="">
              <i class="material-icons">content_paste</i>
              <p>Product Table</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('invoices') ?>">
              <i class="material-icons">assignment</i>
              <p>Invoices Table </p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Products Table</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">

          <form action="<?= site_url('/barang/search/') ?>" method="get">
            <div class="input-group no-border">
              <input type="search" value="" class="form-control" placeholder="Search..." id="search" name="search">
              <button type="submit" class="btn btn-white btn-round btn-just-icon">
                <i class="material-icons">search</i>
                <div class="ripple-container"></div>
              </button>
            </div>
          </form>
          
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">notifications</i> Notifications
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log out</a>
                </div>
              </li>
              <!-- your navbar here -->
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <!-- your content here -->
          <div class="row">
            <div class="col-md-12">

          <!-- Breadcumb -->
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item "><a href="<?= site_url('admin') ?>">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Product Table</li>
              </ol>
            </nav>

            <?php if( $this->session->flashdata('info_tambah') ) : ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?= $this->session->flashdata('info_tambah'); ?>
                <button type="button" class="close mt-2" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php endif ?>

            <?php if( $this->session->flashdata('info_hapus') ) : ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?= $this->session->flashdata('info_hapus'); ?>
                <button type="button" class="close mt-2" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php endif; ?>

            <?php if( $this->session->flashdata('info_ubah') ) : ?>
              <div class="alert alert-success alert-dismissible fade show" role="alert">
                <strong><?= $this->session->flashdata('info_ubah'); ?></strong>
                <button type="button" class="close mt-2" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <?php endif ?>

              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">List of Items</h4>
                  <p class="card-category"> This is list of items</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table table-hover">
                      <thead class="text-primary text-center">
                        <th><b>No</b></th>
                        <th><b>Picture</b></th>
                        <th><b>Name of Items</b></th>
                        <th><b>Brands</b></th>
                        <th><b>Category</b></th>
                        <th><b>Quantity</b></th>
                        <th><b>Price</b></th>
                        <th><b>Warranty</b></th>
                        <th><b>Action</b></th>
                      </thead>
                      <tbody class="text-center">
                      <?php if (empty($barang)) { ?>
                        <tr>
                          <td align="center" colspan="9">Maaf, Data tidak ditemukan</td>
                        </tr>
                      <?php } else { ?>
                      <?php $i = 1; foreach ($barang as $key) : ?>
                        <tr>
                          <th scope="row" class="align-middle"><?= $i; ?></th>
                          <td class="align-middle"><img src="<?= base_url('assets/img/') . $key->gambar; ?>" width="100" class="img-thumbnail"></td>
                          <td class="text-center align-middle"><?= $key->nama_barang ?></td>
                          <td class="align-middle"><?= $key->merk ?></td>
                          <td class="align-middle"><?= $key->kategori ?></td>
                          <td class="align-middle"><?= $key->jumlah ?></td>
                          <td class="align-middle">Rp. <?= number_format( $key->harga,0,",","." ) ?></td>
                          <td class="align-middle"><?= $key->garansi ?></td>
                          <td class="align-middle">

                          <a href="<?= base_url('barang/edit/' . $key->id_barang); ?>" rel="tooltip" title="Edit Product" class="btn btn-info btn-sm"><i class="material-icons">edit</i></a>
                          <a href="<?= base_url('barang/delete/' . $key->id_barang); ?>"rel="tooltip" title="Delete Product" onclick="return confirm('Data yang dihapus akan hilang selamanya, anda yakin?');" class="btn btn-danger btn-sm"><i class="material-icons">delete_forever</i></a>
                          </td>
                        </tr>
                        <?php $i++; endforeach; ?>
			                	<?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
            </table>
          </div>
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="">
                  REKWEB
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script> built by
            <a href="" target="_blank">Selembung</a>
          </div>
          <!-- your footer here -->
        </div>
      </footer>
    </div>
  </div>