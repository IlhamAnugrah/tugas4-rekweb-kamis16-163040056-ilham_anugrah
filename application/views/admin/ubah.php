<div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="white" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
      <div class="logo">
        <a href="" class="simple-text logo-normal">
          Rest Client
        </a>
      </div>
      <div class="sidebar-wrapper">
        <ul class="nav">
          <li class="nav-item  ">
            <a class="nav-link" href="<?= site_url('barang/tambah') ?>">
              <i class="material-icons">note_add</i>
              <p>Add Data</p>
            </a>
          </li>
          <li class="nav-item active ">
            <a class="nav-link" href="<?= site_url('barang') ?>">
              <i class="material-icons">content_paste</i>
              <p>Product Table</p>
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= site_url('invoices') ?>">
              <i class="material-icons">assignment</i>
              <p>Invoices Table </p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top ">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <a class="navbar-brand" href="#pablo">Change Item Data</a>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
              <li class="nav-item">
                <a class="nav-link" href="#pablo">
                  <i class="material-icons">notifications</i> Notifications
                </a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i>
                  <p class="d-lg-none d-md-block">
                    Account
                  </p>
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="#">Profile</a>
                  <a class="dropdown-item" href="#">Settings</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log out</a>
                </div>
              </li>
              <!-- your navbar here -->
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <!-- your content here -->
          <div class="row">
            <div class="col-md-12">

            <!-- Breadcumb -->
            <nav aria-label="breadcrumb" role="navigation">
              <ol class="breadcrumb bg-light">
                <li class="breadcrumb-item "><a href="<?= site_url('admin') ?>">Home</a></li>
                <li class="breadcrumb-item "><a href="<?= site_url('barang') ?>">Product Table</a></li>
                <li class="breadcrumb-item active" aria-current="page">Change Item</li>
              </ol>
            </nav>
            
              <div class="card">
                <div class="card-header card-header-primary">
                  <h4 class="card-title ">Change Item Data</h4>
                  <p class="card-category"> This is change item data page</p>
                </div>
                <div class="card-body">
                  <div class="table-responsive">
                  <div class="card-body">
                    <?= form_open('barang/update'); ?>
                    <?= form_hidden('id_barang', $barang[0]->id_barang) ?>
                        <div class="form">
                        <div class="row ml-1 mr-1">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label for="nama_barang" class="bmd-label-floating">Name of Item</label>
                              <input type="text" id="nama_barang" name="nama_barang" class="form-control" value="<?= $barang[0]->nama_barang ?>">
                            </div>
                          </div>
                        </div>
                        <div class="row ml-1 mr-1">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label for="merk" class="bmd-label-floating">Brand</label>
                              <input type="text" id="merk" name="merk" class="form-control" value="<?= $barang[0]->merk ?>">
                            </div>
                          </div>
                          <div class="col-md-6">
                          <div class="form-group">
                          <label for="kategori" class="bmd-label-floating">Category</label>
                            <input type="text" id="kategori" name="kategori" class="form-control" value="<?= $barang[0]->kategori ?>">
                          </div>
                        </div>
                      </div>
                      <div class="row ml-1 mr-1">
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="jumlah" class="bmd-label-floating">Quantity</label>
                            <input type="number" id="jumlah" name="jumlah" class="form-control" value="<?= $barang[0]->jumlah ?>">
                          </div>
                        </div>
                        <div class="col-md-6">
                          <div class="form-group">
                            <label for="garansi" class="bmd-label-floating">Warranty</label>
                            <input type="text" id="garansi" name="garansi" class="form-control" value="<?= $barang[0]->garansi ?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="harga" class="bmd-label-floating">Price</label>
                          <input type="number" id="harga" name="harga" class="form-control" value="<?= $barang[0]->harga ?>">
                        </div>
                      </div>
                      <div class="col-md-12">
                        <div class="form-group">
                          <label for="gambar" class="bmd-label-floating">Picture</label>
                          <input type="text" id="gambar" name="gambar" class="form-control" value="<?= $barang[0]->gambar ?>">
                        </div>
                      </div>
                      <div class="pull-right">
                        <div class="form-group">
                            <button type="submit" name="submit" id="submit" class="btn btn-primary">
                            <i class="material-icons">save</i>  Save
                            </button>
				                    <a href="<?= base_url('barang') ?>"><button type="button" class="btn">Cancel</button></a>
                        </div>
                      </div>
                      
                        </div>
                    </form>
                  </form>
                </div>
            </div>
        </div
