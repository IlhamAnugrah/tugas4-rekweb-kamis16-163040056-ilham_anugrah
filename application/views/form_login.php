<div class="container" style="height: 100%; padding-top: 100px;">
		<div class="row mt-5">
			<div class="col-md-4 mx-auto">
					<div class="card card-nav-tabs">
						<h4 class="card-header card-header-primary text-center ">Please Sign In</h4>
					<div class="card-body">
					<div class="text-danger"><?= validation_errors() ?></div>
					<div class="text-danger text-center"><b><?= $this->session->flashdata('error') ?></b></div>
					<?= form_open("login"); ?>
						<div class="form-group">
						    <label for="username" class="bmd-label-floating">Username</label>
						    <input type="text" class="form-control" id="username" name="username">
						    <small id="username" class="form-text text-muted">We'll never share your username with anyone else.</small>
					  </div>
						<div class="form-group">
						    <label for="password" class="bmd-label-floating">Password</label>
						    <input type="password" class="form-control" id="password" name="password">
						</div>
						  <button type="submit" class="btn btn-primary pull-right">Sign In</button>
					<?php echo form_close(); ?>
					</div>
				</div>
			</div>
		</div>