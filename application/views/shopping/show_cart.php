<nav class="navbar navbar-expand-lg bg-primary">
  <div class="container">
    <a class="navbar-brand" href="<?= base_url('shopping') ?>"><b>Electronical Shop</b></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    <span class="navbar-toggler-bar navbar-kebab"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav ml-auto">

    <?php if ( $this->session->userdata('username') ) { ?>
      
        <li class="nav-item active">
          <?= anchor('customer/payment_confirmation/', 'Payment Confirmation', [
                    'class' => 'nav-link',
                    'role'  => 'a'
          ]) ?>
        </li>
        <li class="nav-item active">
          <?= anchor('customer/shopping_history/', 'History', [
                     'class' => 'nav-link',
                     'role'  => 'link'
          ]) ?>
        </li>
        <?php } ?>
        <li class="nav-item active">
          <a class="nav-link" href="<?= site_url('shopping/cart') ?>">Shopping Cart
            <i class="material-icons">shopping_cart</i>
            <span class="notification"><?= $this->cart->total_items() ?></span>
          </a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link" href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i class="material-icons">person</i>
            <p class="d-lg-none d-md-block">
              Account
            </p>
          </a>
          <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
            <a class="dropdown-item" href="#">Profile</a>
            <a class="dropdown-item" href="#">Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="<?= site_url('logout'); ?>">Log out</a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

<div class="container mt-5">
	<div class="row">

		<?php if( $cart = $this->cart->contents() ) { ?>
		
		<div class="col-md-12">
			<?php if( $this->session->flashdata('error') ) : ?>
				<div class="alert alert-danger alert-dismissible fade show" role="alert">
					<strong><?= $this->session->flashdata('error'); ?></strong>
					<button type="button" class="close mt-2" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
			<?php endif ?>
		</div>

		<div class="card">
			<div class="card-header card-header-primary">
				<h4 class="card-title">List of Items</h4>
				<p class="card-category">This is list of items</p>
			</div>
			<div class="card-body">
				<div class="table-responsive">
					<table class="table table-bordered">
						<thead class="text-primary text-center">
							<tr>
								<th class="text-center" style="font-weight: bold;">No</th>
								<th class="text-center" style="font-weight: bold;">Product</th>
								<th class="text-center" style="font-weight: bold;">Quantity</th>
								<th class="text-center" style="font-weight: bold;">Price</th>
								<th class="text-center" style="font-weight: bold;">Total</th>
								<th class="text-center" style="font-weight: bold;">Action</th>
							</tr>
						</thead>
						<tbody>
						<?= form_open('shopping/update_cart'); ?>
							<?php 
								$i = 0;
								foreach ($this->cart->contents() as $items):
								$i++; 
							?>
							<?= form_hidden($i.'[rowid]', $items['rowid']); ?>
							<tr>
								<td align="center"><?= $i ?></td>
								<td align="center"><?= $items['name'] ?></td>
								<td align="center"><?php echo form_input(array('name' => $i.'qty', 'value' => $items['qty'], 'max' => '100', 'type' => 'number')); ?></td>
								<td align="center">Rp. <?= number_format( $items['price'],0,",","." ) ?></td>
								<td align="center">Rp. <?= number_format( $items['subtotal'],0,",","." ) ?></td>
								<td align="center">
									<a href="<?= base_url('shopping/delete_product_cart/' . $items['rowid'])?>" class="btn btn-sm btn-danger" rel="tooltip" title="Delete this from cart">
									<i class="material-icons">delete_forever</i></a>
								</td>
							</tr>
						<?php endforeach ?>
						</tbody>
						<tfoot>
							<tr>
								<td align="center" colspan="4" style="font-weight: bold;">TOTAL</td>
								<td align="center" colspan="2" style="font-weight: bold;">Rp. <?= number_format( $this->cart->total(),0,",","." ) ?></td>
							</tr>
						</tfoot>
			</table>
				</div>
			</div>
		</div>
		
			<div class="mx-auto" align="center">
				<?= anchor('shopping/clear_cart', 'Clear Cart', ['class' => 'btn btn-danger']) ?>
				<?= anchor('shopping/index', 'Continue Shopping', ['class' => 'btn btn-primary']) ?>
				<?= anchor('order', 'Check Out', ['class' => 'btn btn-success']) ?>
				<p><?= form_submit('', 'Update your Cart', "class = 'btn btn-warning'"); ?></p>
			</div>
			<?php } else { ?>
			<div class="container mt-5">
				<div class="row" style="padding-top: 100px;">
					<div class="alert alert-danger mx-auto" role="alert">
			<?php 
			echo "<h4>Your shopping cart is still empty!</h4>";	
			echo "<p>You have to buy something first.</p><hr>";
			 ?>
			 <a href="<?= site_url('shopping') ?>">
				 <button class="btn btn-info">
				 	<i class="material-icons text-white">shopping_cart</i> Shop Now
				 </button>
			 </a>
			 <?php } ?>
				</div>
			</div>
			  
		</div>
	</div>
</div>