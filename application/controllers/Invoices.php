<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoices extends CI_Controller {

    public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/Tugas3-Rekweb-Kamis16-163040056-Ilham_Anugrah";
	}

	public function index()
	{
		$data['judul'] = "Latihan Rest API";
		$data['invoice'] = json_decode( $this->curl->simple_get( $this->API . '/invoices/') );
		$data['ivc'] = $data['invoice'];
		$data['content'] = 'admin/invoices';

		$this->load->view('templates/template', $data);
    }
    
    public function detail($id)
	{
		$data['judul'] = "Latihan Rest API";
		$data['invoice'] = json_decode( $this->curl->simple_get( $this->API . '/invoices_detail/?invoice_id=' . $id) );
		$data['orders'] = $data['invoice'];
		$data['content'] = 'admin/invoices_detail';

		$this->load->view('templates/template', $data);
	}
    
} 