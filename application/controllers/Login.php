<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/Tugas3-Rekweb-Kamis16-163040056-Ilham_Anugrah";
	}

    public function index() {
		$data['judul'] = 'Halaman Login';
		$password = $this->input->post('password');
		$this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
		$this->form_validation->set_rules('password', 'Password', 'required|alpha_numeric');

		if( $this->form_validation->run() == FALSE ) {
            $data['content'] = 'form_login';
		    $this->load->view('templates/template', $data);
		} else {
			
			$valid_user = json_decode( $this->curl->simple_get( $this->API . '/login/?password='. $password));

			if ($valid_user > 0) {
				$this->session->set_userdata('username', $valid_user[0]->username);
				$this->session->set_userdata('level', $valid_user[0]->level);
				switch ($valid_user[0]->level) {
					case 1: redirect(base_url('Barang')); break;
					case 2: redirect(base_url('shopping')); break;
					default: break;
				}
			} else {
				$this->session->set_flashdata('error', 'Wrong username / password');
				redirect('login');
			}
		}
    }
    
    public function logout() {
		$this->session->sess_destroy();
		redirect('login');
	}

}