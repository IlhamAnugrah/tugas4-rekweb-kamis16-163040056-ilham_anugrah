<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Barang extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/Tugas3-Rekweb-Kamis16-163040056-Ilham_Anugrah";
	}

	public function index()
	{
		$data['judul'] = "Latihan Rest API";
		$data['barang'] = json_decode( $this->curl->simple_get( $this->API . '/barang/') );
		$data['brg'] = $data['barang'];
		$data['content'] = 'admin/barang';

		$this->load->view('templates/template', $data);
	}

	public function search()
	{
		$search = urldecode($this->input->get("search", true));

		$data['judul'] = "Latihan Rest API";
		$data['barang'] = json_decode($this->curl->simple_get( $this->API . '/barang/search/?cari=' . urldecode($search)) );
		$data['brg'] = $data['barang'];
		$data['content'] = 'admin/barang';

		$this->load->view('templates/template', $data);
	}

	public function tambah()
	{
		if( $this->input->post() ) {

		$nama_barang = $this->input->post('nama_barang');
		$merk = $this->input->post('merk');
		$kategori = $this->input->post('kategori');
		$jumlah = $this->input->post('jumlah');
		$garansi = $this->input->post('garansi');
		$harga = $this->input->post('harga');
		$gambar = $this->input->post('gambar');

		$data = array(
				"nama_barang" => $nama_barang,
				"merk" => $merk,
				"kategori" => $kategori,
				"jumlah" => $jumlah,
				"garansi" => $garansi,
				"harga" => $harga,
				"gambar" => $gambar
			);
        		 
		$insert = $this->curl->simple_post($this->API . '/barang/', $data, array(CURLOPT_BUFFERSIZE => 10));
		// echo json_encode(array("status" => TRUE));
		if( $insert ) {
			$this->session->set_flashdata('info_tambah', 'Data berhasil ditambahkan!');
		} else{
			$this->session->set_flashdata('info_tambah', 'Data gagal ditambahkan!');
		}
		redirect('barang');
	}
		$data['judul'] = 'Add Item Page';
		$data['content'] = 'admin/tambah';
        $this->load->view('templates/template', $data);
	}

	public function edit($detail)
	{
		$id = $detail;
		$params = array("id_barang" => $id);
		$data['barang'] = json_decode($this->curl->simple_get($this->API .'/barang', $params));
		$barang = $data['barang'];
		$json = json_encode(array("status" => 200, "brg" => $barang));
		// echo $json;

		$data['judul'] = 'Change Item Page';
		$data['content'] = 'admin/ubah';
		$this->load->view('templates/template', $data);
	}

	public function update()
	{	
		if( $this->input->post() ) {

		$nama_barang = $this->input->post('nama_barang');
		$merk = $this->input->post('merk');
		$kategori = $this->input->post('kategori');
		$jumlah = $this->input->post('jumlah');
		$garansi = $this->input->post('garansi');
		$harga = $this->input->post('harga');
		$gambar = $this->input->post('gambar');
		$id = $this->input->post("id_barang");

		$data = array(
			"nama_barang" => $nama_barang,
			"merk" 		  => $merk,
			"kategori" 	  => $kategori,
			"jumlah" 	  => $jumlah,
			"garansi" 	  => $garansi,
			"harga" 	  => $harga,
			"gambar" 	  => $gambar,
			"id_barang"	  => $id 
		);
	
		$update = $this->curl->simple_put( $this->API . '/barang/', $data, array(CURLOPT_BUFFERSIZE => 10) );
		$data['barang'] = json_decode($this->curl->simple_get($this->API . '/barang'));
		// echo json_decode(array("status" => TRUE));

		if( $update ) {
			$this->session->set_flashdata('info_ubah', 'Data berhasil diubah!');
		} else{
			$this->session->set_flashdata('info_ubah', 'Data gagal diubah!');
		}
		redirect('barang');
	}
		$data['judul'] = 'Change Item Page';
		$data['content'] = 'admin/ubah';
		$this->load->view('templates/template', $data);
	}

	public function delete($id)
	{
		// $id = $this->input->post('id_barang');
		$delete = $this->curl->simple_delete($this->API .'/barang/', array('id_barang' => $id), array(CURLOPT_BUFFERSIZE => 10));
		echo json_encode(array("status" => TRUE));

		if( $delete ) {
			$this->session->set_flashdata('info_hapus', 'Data berhasil dihapus!');
		} else{
			$this->session->set_flashdata('info_hapus', 'Data gagal dihapus!');
		}

		redirect('barang');
	}
}
