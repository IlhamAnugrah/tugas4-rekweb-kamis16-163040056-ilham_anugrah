<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Order extends CI_Controller {

    public function __construct() {
		parent::__construct();
		if ( !$this->session->userdata('username') ) {
			redirect('login');
		}
	}

	public function index() {
		$is_processed = json_decode( $this->curl->simple_get( $this->API . '/barang/') );
		if ( $is_processed ) {
            $this->session->set_flashdata('error', 'Failed to processed your order, please try again!');
			redirect('shopping/cart');
		} else {
			$this->cart->destroy();
			redirect('order/success');
		}
    }

    public function success() {
        $data['judul'] = 'Shopping';
        
		$data['content'] = 'shopping/order_success';
		$this->load->view('templates/template', $data);
	}
    
}