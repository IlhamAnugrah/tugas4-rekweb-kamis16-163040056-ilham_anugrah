<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Shopping extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->API = "http://localhost/Tugas3-Rekweb-Kamis16-163040056-Ilham_Anugrah";
	}

	public function index()
	{
		$data['judul'] = "Latihan Rest API";
		$data['barang'] = json_decode( $this->curl->simple_get( $this->API . '/barang/') );
		$data['brg'] = $data['barang'];
		$data['content'] = 'shopping/index';

		$this->load->view('templates/template', $data);
    }
    
    public function add_to_cart($id) {
		$barang = json_decode( $this->curl->simple_get( $this->API . '/barang/?id_barang=' . $id));

		$data = array(
	        'id'      => $barang[0]->id_barang,
	        'qty'     => 1,
	        'price'   => $barang[0]->harga,
	        'name'    => $barang[0]->nama_barang
	    );

        $this->cart->insert($data);
            redirect('shopping');
    }

    public function cart() {
		$data['judul'] = 'Shopping';
		
		$data['content'] = 'shopping/show_cart';
		
		$this->load->view('templates/template', $data);
    }
    
    public function clear_cart() {
		$this->cart->destroy();
		redirect('shopping');
    }
    
    public function update_cart() {
		$data['judul'] = 'Shopping';

		$i = 1;
		
		foreach ( $this->cart->contents() as $barang ) {
			$getData = json_decode( $this->curl->simple_get( $this->API . '/barang/'));
			if ( $getData[0]->jumlah < $_POST[$i . 'qty'] ) {
				$this->session->set_flashdata('error', 'Stock is not enough');
				redirect('shopping/cart');
			} else {
				$this->cart->update(array('rowid' => $barang['rowid'], 'qty' => $_POST[$i . 'qty']));
				$i++;
			}
		}
		$data['content'] = 'shopping/show_cart';
		$this->load->view('templates/template', $data);
	}

	function delete_product_cart($rowid) 
	{
		if ($rowid=="all") {
			$this->cart->destroy();
		} else {
			$data = array('rowid' => $rowid,
							'qty' =>0);
			$this->cart->update($data);
		}
		redirect('shopping/cart');
	}
    
}
